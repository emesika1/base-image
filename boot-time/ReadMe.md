# Boot Time Measurement Test

Per ISO 26262 automotive infotainment is held to strict boot times for certain features. Components for example like 
rearview camera (rvc) and automotive safety component signal processing must be available to display or forward within
strict requirements. This boot time enforcement will evolve identifed features only but not the entire OS boot process.
A part of this optimization will be measuring and maintaining progress on these boot times as improvements are implemented.

## To run the tests locally
1. `tmt run -a -vvv plan --name boot-time`

## To run the tests on provision- virtual:
1.  `tmt run -a -vvv plan --name boot-time provision --how virtual --image http://cloud.centos.org/centos/8-stream/x86_64/images/CentOS-Stream-GenericCloud-8-20210603.0.x86_64.qcow2`

## This Test Suite includes 2 tests:

Setup for which includes installing required packages (python3, xorg, gdm, neptune3-ui_copr) and configuring a 
xorg dummy video driver. The test also configures an edge user to auto boot this environment.
After these configurations take place, the test utilizes tmt-reboot to obtain the startup timings after being enabled

1. boot_time:
Currently using systemd-analyze a built-in component requiring no additional changes to the base image allows for measuring
kernel, initrd, userspace and total boot time. Future improvement would include submission to a tool like Elasticsearch for 
visual trend tracking

2. Neptune boot time:
The cluster is a core feature to any automotive infotainment system. The test sets the environment variable to enable 
neptune3-ui startup timing for each component within the neptune3-ui software

## The metrics for these tests are collected using :
1. `systemd-analyze` for listing system boot time values
2. `neptune AM_STARTUP_TIMER=1` ENV variable enabling logging of neptune startup values to journald
3. `python regex module` for identifying output values and capturing for logging
