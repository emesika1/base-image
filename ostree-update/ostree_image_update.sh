#!/bin/bash

set -uxo pipefail

if [ ! -z "$S3_BUCKET_NAME" ] && [ ! -z "$AWS_REGION" ]; then
   BASE_REPO_URL="https://${S3_BUCKET_NAME}.s3.${AWS_REGION}.amazonaws.com/repo/${IMAGE_KEY}.repo"
   AWS_INDEX=index.html
fi

IS_REPO_EXIST=$(curl -I -w "%{http_code}" "$BASE_REPO_URL/$AWS_INDEX" | tail -1)

# Check if 200 other exit
if [ "$IS_REPO_EXIST" != "200" ]; then 
  echo "$BASE_REPO_URL" returned "$IS_REPO_EXIST" >&2
  exit 1
fi

#In case Pipeline is running the test $BUILD_TARGET set as env var
if [ -z "$S3_BUCKET_NAME" ] && [ -z "$AWS_REGION" ]; then
   TREE_REPO="${OS_PREFIX}${OS_VERSION}-${BUILD_TARGET}-${IMAGE_NAME}-ostree${IMAGE_ANNOTATION}.${ARCH}.repo"
   BASE_REPO_URL+="/$TREE_REPO"
   echo "Updated BASE_REPO_URL: to  $BASE_REPO_URL" >&2
fi

REPO_COMMIT_REF="${OS_PREFIX}${OS_VERSION}/${ARCH}/${BUILD_TARGET}-${IMAGE_NAME}"

echo "Curl $BASE_REPO_URL receive NEW_SHA_ID"
NEW_SHA_ID=$(curl "$BASE_REPO_URL"/refs/heads/"$REPO_COMMIT_REF")  
# Check not empty
if [ -z "$NEW_SHA_ID" ] || [ "$NEW_SHA_ID" == "" ]; then
  echo "NEW_SHA_ID is empty please check"
  echo "$BASE_REPO_URL"/refs/heads/"$REPO_COMMIT_REF"
  exit 127
fi

OLD_SHA_ID=$(ostree admin status | grep '\*' | awk '{print $3}')

if [ "$OLD_SHA_ID" == "$NEW_SHA_ID" ]; then
  echo "NEW_SHA_ID equals to old please check you repo config" >&2
  exit 1
fi

# set through ostree, upstream repo 
ostree remote add --no-gpg-verify  upstream "$BASE_REPO_URL" 

# print for debug
ostree remote show-url upstream

# cs9 depends on build type. x86_64, aarch64, qcow/raw
ostree admin switch upstream:"$REPO_COMMIT_REF"

#print for debug
ostree admin status

# check ostree admin status updates with NEW_SHA_ID
UPDATED_SHA_ID=$(ostree admin status| grep 'pending' | awk '{print $2}')

if [[ "$UPDATED_SHA_ID" != "$NEW_SHA_ID"* ]]; then
  echo "NEW_SHA_ID in repo $NEW_SHA_ID " >&2
  echo "is different from pending UPDATED_SHA_ID $UPDATED_SHA_ID" >&2
  exit 1
fi

echo "$UPDATED_SHA_ID" > /root/.UPDATED_SHA_ID
